﻿using fsDataProcessingTower.models;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace fsDataProcessingTower.mappers
{
    public class towerStrikeMapper
    {
        //Maps a json response from matlab to the concrete class
        public static strikes mapStrike(string jObject, towerCollectionConfiguration collectionConfig)
        {
            //Parse jobject
            JToken jStrike = JObject.Parse(jObject);

            //Cast rawData property to a JArray
            JArray sRawData = (JArray)jStrike["rawData"];

            //Create a double[] instance from the JArray
            short[] dRawData = sRawData.ToObject<short[]>();

            //Convert each double value to a byte for storage
            byte[] bRawData = dRawData.SelectMany(s => BitConverter.GetBytes(s)).ToArray();

            short numberOfActiveGages = (short)collectionConfig.activeGauges.Where(g => g).Count();

            //Map the data to a new strike object
            return new strikes
            {
                rawData = bRawData,
                isBad = (long)jStrike["status"],
                isClipped = (long)jStrike["clipped"],
                isPreTrig = (long)jStrike["pretrig"],
                isLowAmp = (long)jStrike["lowAmp"],
                isBadRodReflection = (long)jStrike["badRodReflection"],
                strikeNumber = collectionConfig.strikeNumber.Value,
                timestamp = DateTime.Now.ToString(),
                strikingObject = collectionConfig.strikingObject,
                numberOfActiveGages = numberOfActiveGages,
                testId = collectionConfig.testId.Value,
            };
        }
    }
}