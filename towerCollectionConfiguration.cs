﻿using fsDataProcessingCore;
using System;

namespace fsDataProcessingTower
{
    //Extends collectionConfiguration by adding properties specific to tower
    public class towerCollectionConfiguration : collectionConfiguration
    {
        public string strikingObject { get; set; }

        public override void isValid()
        {
            base.isValid();

            if (string.IsNullOrWhiteSpace(strikingObject))
                throw new ArgumentNullException("strikingObject is null!");
        }
    }
}