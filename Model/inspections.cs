﻿using System.Collections.Generic;

namespace fsDataProcessingTower.models
{
    public partial class inspections
    {
        public inspections()
        {
            tests = new HashSet<tests>();
            pictures = new HashSet<pictures>();
        }

        public long inspectionId { get; set; }
        public long towerId { get; set; }
        public string observedSiteLabel { get; set; }
        public string observedSiteName { get; set; }
        public string projectNumber { get; set; }
        public string startDate { get; set; }
        public string targetDate { get; set; }
        public string completeDate { get; set; }
        public string status { get; set; }
        public string comments { get; set; }
        public virtual towers tower { get; set; }

        public virtual ICollection<tests> tests { get; set; }
        public virtual ICollection<pictures> pictures { get; set; }
    }
}