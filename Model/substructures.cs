﻿using System.Collections.Generic;

namespace fsDataProcessingTower.models
{
    public partial class substructures
    {
        public substructures()
        {
            measurements = new HashSet<measurements>();
            tests = new HashSet<tests>();
        }

        public long substructureId { get; set; }
        public long towerId { get; set; }
        public string substructureType { get; set; }
        public string substructureLabel { get; set; }

        public virtual towers tower { get; set; }
        public virtual ICollection<measurements> measurements { get; set; }
        public virtual ICollection<tests> tests { get; set; }
    }
}