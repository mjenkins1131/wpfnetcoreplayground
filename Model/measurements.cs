﻿namespace fsDataProcessingTower.models
{
    public partial class measurements
    {
        public long measurementId { get; set; }
        public long substructureId { get; set; }
        public string measurementKey { get; set; }
        public double measurementValue { get; set; }
        public string measurementUnit { get; set; }

        public virtual substructures substructures { get; set; }
    }
}