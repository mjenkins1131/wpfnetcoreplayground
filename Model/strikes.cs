﻿using Newtonsoft.Json;

namespace fsDataProcessingTower.models
{
    public partial class strikes
    {
        [JsonIgnore]
        public long strikeId { get; set; }

        [JsonIgnore]
        public long testId { get; set; }

        [JsonIgnore]
        public byte[] rawData { get; set; }

        public long isBad { get; set; }
        public long isClipped { get; set; }
        public long isPreTrig { get; set; }
        public long isLowAmp { get; set; }
        public long isBadRodReflection { get; set; }
        public byte[] chart { get; set; }

        [JsonIgnore]
        public string strikingObject { get; set; }

        [JsonIgnore]
        public long numberOfActiveGages { get; set; }

        public long strikeNumber { get; set; }

        [JsonIgnore]
        public string timestamp { get; set; }

        [JsonIgnore]
        public virtual tests test { get; set; }
    }
}