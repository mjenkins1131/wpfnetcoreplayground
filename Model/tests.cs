﻿using System.Collections.Generic;

namespace fsDataProcessingTower.models
{
    public partial class tests
    {
        public tests()
        {
            strikes = new HashSet<strikes>();
        }

        public long testId { get; set; }
        public long substructureId { get; set; }
        public long inspectionId { get; set; }
        public string status { get; set; }

        public virtual substructures substructures { get; set; }
        public virtual inspections inspection { get; set; }
        public virtual ICollection<strikes> strikes { get; set; }
    }
}