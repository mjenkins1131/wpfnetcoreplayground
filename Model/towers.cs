﻿using System.Collections.Generic;

namespace fsDataProcessingTower.models
{
    public partial class towers
    {
        public towers()
        {
            inspections = new HashSet<inspections>();
            substructures = new HashSet<substructures>();
        }

        public long towerId { get; set; }
        public string siteLabel { get; set; }
        public string siteName { get; set; }
        public string siteStreet1 { get; set; }
        public string siteStreet2 { get; set; }
        public string siteCity { get; set; }
        public string siteStateOrProvince { get; set; }
        public string siteCountry { get; set; }
        public string sitePostalCode { get; set; }
        public string towerType { get; set; }
        public string towerTypeAbrv { get; set; }
        public string concreteVisibilityType { get; set; }
        public string concretePierTopShape { get; set; }
        public string concretePierTopOrientation { get; set; }
        public string foundationType { get; set; }
        public string comments { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public long? gradeBeams { get; set; }
        public long? step { get; set; }
        public long? pccOrMudmat { get; set; }


        public virtual ICollection<inspections> inspections { get; set; }
        public virtual ICollection<substructures> substructures { get; set; }
    }
}