﻿namespace fsDataProcessingTower.models
{
    public class pictures
    {
        public long pictureId { get; set; }
        public byte[] picture { get; set; }
        public string pictureLabel { get; set; }
        public long inspectionId { get; set; }

        public virtual inspections inspection { get; set; }
    }
}