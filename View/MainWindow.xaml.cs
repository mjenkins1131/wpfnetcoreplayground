﻿using fsDataProcessingCore;
using fsDataProcessingTower;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfNetCorePlayGround.Model_View;

namespace WpfNetCorePlayGround
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int strike = 0;
        towerController towerController = new towerController();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            bool[] gages = new bool[] { false, true, false, false, false, false };

            collectionConfiguration col = new towerCollectionConfiguration();
            col.posttrig = 10000;
            col.pretrig = 1000;
            col.timestep = 1;
            col.voltvalue = 2;
            col.testId = 1;
            col.level = 0.2;
            col.activeGauges = gages;
            col.trigchannel = 2;
            col.strikeNumber = 1;
            col.inspectionId = 1;
            col.testId = 1;
            col.substructureId = 1;

            strike++;
            byte [] chart = towerController.startDataCollection(col);

            //string imrebase64data = Convert.ToBase64String(chart);
            //string imgDataUrl = string.Format("data:image/png;base64,{0}", imrebase64data);

            //var binData = System.Convert.FromBase64String(imrebase64data);

            BitmapImage image = new BitmapImage();
            using (var mem = new MemoryStream(chart))
            {
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();


            plotImage.Source=null;
            plotImage.Source = image;

            resultsBox.Text = "DONE";

        }

        private void btnLeftMenuHide_Click(object sender, RoutedEventArgs e)
        {
            ShowHideMenu("sbHideLeftMenu", btnLeftMenuHide, btnLeftMenuShow, pnlLeftMenu);
        }

        private void btnLeftMenuShow_Click(object sender, RoutedEventArgs e)
        {
            ShowHideMenu("sbShowLeftMenu", btnLeftMenuHide, btnLeftMenuShow, pnlLeftMenu);
        }

        private void ShowHideMenu(string Storyboard, Button btnHide, Button btnShow, StackPanel pnl)
        {
            Storyboard sb = Resources[Storyboard] as Storyboard; 
            sb.Begin(pnl);

            if (Storyboard.Contains("Show"))
            {
                btnHide.Visibility = System.Windows.Visibility.Visible;
                btnShow.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (Storyboard.Contains("Hide"))
            {
                btnHide.Visibility = System.Windows.Visibility.Hidden;
                btnShow.Visibility = System.Windows.Visibility.Visible;
            }
        }
    }
}
