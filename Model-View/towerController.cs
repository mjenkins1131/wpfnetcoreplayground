﻿using fsDataProcessingCore;
using fsDataProcessingCore.helpers;
using fsDataProcessingTower;
using fsDataProcessingTower.mappers;
using fsDataProcessingTower.models;
using fsDataProcessingTower.proxies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WpfNetCorePlayGround.Model_View
{

    class towerController
    {
        public byte[]  startDataCollection(collectionConfiguration collectionConfig)
        {
            
            string path = Directory.GetCurrentDirectory() + @"\testData";

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }


                char[,] result = towerMatLabProxy.collectAndCheckData(collectionConfig);

            //Get specific dimension of the result
            string strike = string.Join(string.Empty, (char[])multiDimensionArrayHelper.getDimension(result, 1));

            //Map the JSON to the model
            strikes mappedStrike = towerStrikeMapper.mapStrike(strike, (towerCollectionConfiguration)collectionConfig);

            //Cast strike data to short to be able to pass into matlab to generate a chart
            short[] strikeData = Enumerable.Range(0, mappedStrike.rawData.Length / sizeof(short))
                .Select(offset => BitConverter.ToInt16(mappedStrike.rawData, offset * sizeof(short))).ToArray();

            //Get chart of strike from matlab
            byte[,] chartPlot = towerMatLabProxy.plotToByteArray(strikeData.Take(10000).ToArray(), (short)collectionConfig.activeGauges.Where(g => g).Count());

            mappedStrike.chart = extractByteChart(chartPlot);

            string filename1 = "strike_" + DateTime.Now.ToString("MMMM_dd_yyyy_hh_mm_ss")+ ".io$";
            string filename2 = "strike_" + DateTime.Now.ToString("MMMM_dd_yyyy_hh_mm_ss") + ".iot";

            using (StreamWriter fs = new StreamWriter(Path.Combine(path, filename1)))
            {
                fs.WriteLine("*TEXT");
                fs.WriteLine("DaqBoard3005USB");
                fs.WriteLine("*VERSION");
                fs.WriteLine("1.00");
                fs.WriteLine("*FORMAT");
                fs.WriteLine("TEXT");
                fs.WriteLine("*FREQUENCY");
                fs.WriteLine("1000000.000000, 1000000.000000");
                fs.WriteLine("*PRETRIG_COUNT");
                fs.WriteLine(collectionConfig.pretrig);
                fs.WriteLine("*CHANNELS");
                fs.WriteLine("00000,1.000000,CH01,V,0.000000");
                fs.Close();

            }
            
            using (StreamWriter fs2 = new StreamWriter(Path.Combine(path, filename2)))
            {
                for (int i = 0; i < strikeData.Count(); i++)
                {
                    fs2.WriteLine(strikeData[i]);
                }
                fs2.Close();

            }

                return mappedStrike.chart;

            //if (mappedStrike.isBad == 1 || mappedStrike.isBadRodReflection == 1 || mappedStrike.isClipped == 1 || mappedStrike.isLowAmp == 1 || mappedStrike.isPreTrig == 1)
            //{
            //    return "BAD";
            //}
            //else
            //{

            //    return "GOOD";

            //}

        }
        protected byte[] extractByteChart(byte[,] chartPlot)
        {
            //Get last dimension of result
            byte[] plotBytes = (byte[])multiDimensionArrayHelper.getDimension(chartPlot, 0);

            //Resize chart
            plotBytes = imageHelper.resizeByteArray(plotBytes, 903, 409);

            return plotBytes;
        }

    }
}
