﻿using fsDataProcessingCore;
using MathWorks.MATLAB.NET.Arrays;
using TowerCollectAndCheckData;

namespace fsDataProcessingTower.proxies
{
    public class towerMatLabProxy
    {
        static towerProxy proxy = new towerProxy();

        //Wrapper method for collectionConfiguration
        public static char[,] collectAndCheckData(collectionConfiguration daqConfig)
        {
            //Get strike data from matlab/daq
            MWCharArray result = (MWCharArray)collectAndCheckData(new MWLogicalArray(daqConfig.activeGauges), daqConfig.pretrig, daqConfig.posttrig, daqConfig.timestep, daqConfig.voltvalue, daqConfig.trigchannel, daqConfig.level);

            return (char[,])result.ToArray();
        }

        //Prompts MATLAB to begin collecting data, and perform checks on the collected data, returns a strike object
        private static MWArray collectAndCheckData(MWArray activeGages, MWArray pretrig, MWArray posttrig, MWArray ts, MWArray voltValue, MWArray trigChannel, MWArray level)
        {
            return proxy.collectAndCheckData(activeGages, pretrig, posttrig, ts, voltValue, trigChannel, level);
        }

        //Takes strike data and creates a chart, returns a byte chart
        public static byte[,] plotToByteArray(short[] strikeData, short numberOfGages)
        {
            MWNumericArray result = (MWNumericArray)plotToByteArray(new MWNumericArray(strikeData), new MWNumericArray(numberOfGages));

            return (byte[,])result.ToArray();
        }

        //Takes strike data and creates a chart, returns a byte chart
        private static MWArray plotToByteArray(MWArray strikeData, MWArray numberOfGages)
        {
            return proxy.plotToByteArray(strikeData, numberOfGages);
        }
    }
}
