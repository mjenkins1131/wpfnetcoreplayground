﻿using System;

namespace fsDataProcessingCore
{
    //Base class for collection configuration, these properties should be arguments common to anything using the daq for data collection
    public class collectionConfiguration
    {
        public long? pretrig { get; set; }
        public long? posttrig { get; set; }
        public long? timestep { get; set; }
        public long? voltvalue { get; set; }
        public double? level { get; set; }
        public bool[] activeGauges { get; set; }
        public long? trigchannel { get; set; }
        public long? strikeNumber { get; set; }
        public long? inspectionId { get; set; }
        public long? testId { get; set; }
        public long? substructureId { get; set; }
       

        //Ensures all properties of the collection config have a proper value
        public virtual void isValid()
        {
            if (!pretrig.HasValue)
                throw new ArgumentNullException("pretrig is null!");

            if (!posttrig.HasValue)
                throw new ArgumentNullException("posttrig is null!");

            if (!timestep.HasValue)
                throw new ArgumentNullException("timestep is null!");

            if (!level.HasValue)
                throw new ArgumentNullException("level is null!");

            if (!trigchannel.HasValue)
                throw new ArgumentNullException("trigchannel is null!");

            if (!voltvalue.HasValue)
                throw new ArgumentNullException("voltvalue is null!");

            if (activeGauges is null || activeGauges.Length != 6)
                throw new ArgumentOutOfRangeException("activeGauges is invalid!");

            if (!strikeNumber.HasValue)
                throw new ArgumentNullException("strikeNumber is null!");

            if (!inspectionId.HasValue)
                throw new ArgumentNullException("inspectionId is null!");

            if (!substructureId.HasValue)
                throw new ArgumentNullException("substructureId is null!");
        }
    }
}