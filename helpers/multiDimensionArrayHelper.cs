﻿using System;
using System.Linq;

namespace fsDataProcessingCore.helpers
{
    //Helper class for common operations we may wish to perform regarding multidimensional arrays
    public static class multiDimensionArrayHelper
    {
        //Returns the specified dimension
        public static Array getDimension(Array array, int dimensionToGet)
        {
            try
            {
                int[] indices = new int[array.Rank];

                indices.Select(i => i = 0);

                Array returnArray = Array.CreateInstance(array.GetType().GetElementType(), array.GetUpperBound(dimensionToGet) + 1);

                Enumerable.Range(0, array.GetUpperBound(dimensionToGet) + 1).ToList().ForEach(i =>
                {
                    indices[dimensionToGet] = i;
                    returnArray.SetValue(array.GetValue(indices), i);
                });

                return returnArray;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}