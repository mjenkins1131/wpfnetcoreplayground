﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;

namespace fsDataProcessingCore.helpers
{
    //Helper class for common operations we may wish to perform regarding byte array images/images
    public class imageHelper
    {
        //Compresses a byte array
        public static byte[] compress(byte[] data, CompressionLevel compressionLevel = CompressionLevel.Optimal)
        {
            MemoryStream output = new MemoryStream();

            using DeflateStream dstream = new DeflateStream(output, compressionLevel);

            dstream.Write(data, 0, data.Length);

            return output.ToArray();
        }

        //Decompresses a byte array
        public static byte[] decompress(byte[] data)
        {
            MemoryStream output = new MemoryStream();

            using DeflateStream dstream = new DeflateStream(new MemoryStream(data), CompressionMode.Decompress);

            dstream.CopyTo(output);

            return output.ToArray();
        }

        //Resizes a byte array
        public static byte[] resizeByteArray(byte[] image, int width, int height)
        {
            using MemoryStream ms = new MemoryStream(image);

            Image plot = Image.FromStream(ms);

            Bitmap bmp = resizeImage(plot, width, height);

            using MemoryStream newImgMs = new MemoryStream();

            bmp.Save(newImgMs, ImageFormat.Png);

            return newImgMs.ToArray();
        }

        //Resizes an images
        public static Bitmap resizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using Graphics graphics = Graphics.FromImage(destImage);

            graphics.CompositingMode = CompositingMode.SourceCopy;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

            using ImageAttributes wrapMode = new ImageAttributes();

            wrapMode.SetWrapMode(WrapMode.TileFlipXY);
            graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);

            return destImage;
        }
    }
}